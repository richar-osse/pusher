from openpyxl import load_workbook
import sys
import os
import csv

def csv_from_excel(xlsx_file_path, csv_file_path, separator=";", quote=None):
    #file_name, extension = os.path.splitext(xlsx_file_path)
    wb = load_workbook(filename=xlsx_file_path)
    first_sheet = wb.get_sheet_names()[0]
    worksheet = wb.get_sheet_by_name(first_sheet)
    content = []
    for row in worksheet.iter_rows():
        my_row = []
        for cell in row:
            value = cell.internal_value
            the_format = cell.number_format
            if value_is_float_in_int_format(value, the_format):  # case when excel will gives 80 instead of 80.0
                value = float(value)
            my_row.append(value)
        content.append(my_row)
    write_csv_file("{}.csv".format(csv_file_path), content, separator, csv.QUOTE_ALL if quote == "1" else csv.QUOTE_NONE)


def value_is_float_in_int_format(value, the_format):
    result = isinstance(value, int)
    result = result and not (the_format == "General" or the_format == "0")
    return result


def write_csv_file(csv_file_path, content, delimiter=";", quote=None):
    with open(csv_file_path, "w") as a_file:
        writer = csv.writer(a_file, lineterminator='\n', delimiter=delimiter, quoting=quote)
        #writer = csv.writer(csv_file, quoting=csv.QUOTE_NONE, delimiter=";") # csv.QUOTE_ALL
        writer.writerows(content)

csv_from_excel(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
